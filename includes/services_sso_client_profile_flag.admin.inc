<?php

function services_sso_client_profile_flag_admin_form($form_state) {
  services_sso_client_profile_flag_load_includes();

  // Pull in some analytics info about flags.
  $info = _services_sso_client_profile_flag_analytics();

  $form = array();

  $form['services_sso_client_profile_flag_endpoint'] = array(
    '#title' => t('Endpoint name'),
    '#description' => t('The name of the endpoint on the Services REST service. Example: profile-flag'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('services_sso_client_profile_flag_endpoint', ''),
  );

  $form['info'] = array(
    '#type' => 'fieldset',
    '#title' => 'Information and statistics',
  );
  $form['info']['stats'] = array(
    '#type' => 'markup',
    '#value' => theme('services_sso_client_profile_flag_info', $info),
  );

  // Cron settings
  $form['cron_batch'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron and batch settings'),
  );
  $form['cron_batch']['services_sso_client_profile_flag_batch_num'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of flags to run at batch'),
    '#size' => 5,
    '#max_length' => 3,
    '#required' => TRUE,
    '#description' => t('The number of flags to process during each increment of the batch operation. Increase or decreaes this number based on the speed of your webserver and the settings of PHP timeout.'),
    '#default_value' => variable_get('services_sso_client_profile_flag_batch_num', 20),
  );
  $form['cron_batch']['services_sso_client_profile_flag_cron_num'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of flags to run on cron'),
    '#size' => 5,
    '#max_length' => 3,
    '#required' => TRUE,
    '#description' => t('The number of flags to process at every cron run. You might want to increase or decrease this number based on the speed of your webserver or the processing load in terms of other cron jobs.'),
    '#default_value' => variable_get('services_sso_client_profile_flag_cron_num', 20),
  );

  // Generate options
  $options = array(
    '1' => t('Immediately'),
    15*60 => t('15 mins'),
    30*60 => t('30 mins'),
    45*60 => t('45 mins'),
    3600 => t('1 hr'),
    3*3600 => t('3 hrs'),
    5*3600 => t('5 hrs'),
    9*3600 => t('9 hrs'),
    12*3600 => t('12 hrs'),
    18*3600 => t('18 hrs'),
    24*3600 => t('1 day'),
    36*3600 => t('36 hrs'),
    48*3600 => t('2 days'),
    72*3600 => t('3 days'),
    7*24*3600 => t('1 week'),
    2*7*24*3600 => t('2 weeks'),
  );
  $form['cron_batch']['services_sso_client_profile_flag_cron_time_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Minimum threshold between flag processing'),
    '#required' => TRUE,
    '#options' => $options,
    '#description' => t('The amount of time between initial flag processing and every subsequent re-processing of that particular flag.'),
    '#default_value' => variable_get('services_sso_client_profile_flag_cron_time_threshold', 60*60*12),
  );

  $form['sitewide'] = array(
    '#type' => 'fieldset',
    '#title' => t('Controls'),
  );
  $form['sitewide']['retrieve_all'] =  array(
    '#type' => 'button',
    '#value' => t('Retrieve all flags'),
    '#executes_submit_callback' => TRUE,
    '#submit' => array('services_sso_client_profile_flag_retrieve_all'),
    '#weight' => 0,
  );
  $form['sitewide']['retrieve_new'] =  array(
    '#type' => 'button',
    '#value' => t('Retrieve only new flags'),
    '#executes_submit_callback' => TRUE,
    '#submit' => array('services_sso_client_profile_flag_retrieve_new'),
    '#weight' => 0,
  );
  // $form['sitewide']['reset'] = array(
  //   '#type' => 'button',
  //   '#value' => t('Reset ALL profile flags to their default configuration on the SSO server'),
  //   '#executes_submit_callback' => TRUE,
  //   '#submit' => array('services_sso_client_profile_flag_reset_all'),
  //   '#weight' => 5,
  // );
  $form['sitewide']['apply_all'] = array(
    '#type' => 'button',
    '#value' => t('Apply all flags'),
    '#executes_submit_callback' => TRUE,
    '#submit' => array('services_sso_client_profile_flag_batch'),
    '#weight' => 5,
  );

  return system_settings_form($form);
}

function services_sso_client_profile_flag_reset_all() {
	services_sso_client_profile_flag_reset(array(), TRUE);
}

function services_sso_client_profile_flag_retrieve_all() {
  module_load_include('php', 'services_sso_client_profile_flag', 'includes/profileFlag.class');

  ProfileFlag::retrieve(TRUE);
}

function services_sso_client_profile_flag_retrieve_new() {
  module_load_include('php', 'services_sso_client_profile_flag', 'includes/profileFlag.class');

  ProfileFlag::retrieveNew(TRUE);
}

/**
 * Return some statistical info.
 */
function _services_sso_client_profile_flag_analytics() {
  $info = array();

  $info['total'] = db_result(db_query("SELECT COUNT(DISTINCT fid) AS count FROM {services_sso_client_profile_flag}"));
  $info['unprocessed'] = db_result(db_query("SELECT COUNT(DISTINCT fid) AS count FROM {services_sso_client_profile_flag} WHERE last_processed < 1"));
  $info['last_retrieval'] = variable_get('services_sso_client_profile_flag_last_retrieval', 0);

  return $info;
}

// function services_sso_client_profile_flag_form_alter(&$form, &$form_state, $form_id) {
//   switch ($form_id) {
//     case 'services_sso_client_profile_flag_actions':
//       break;
//   }
// } 


/**
* Creates flag action assignment interface form.
*
* Uses AHAH Helper module for dynamic reloading of the form elements. For each flag,
* Creates options for adding or removing groups and roles.
* 
*/
function services_sso_client_profile_flag_actions($form_state) {
  services_sso_client_profile_flag_load_includes();

  drupal_add_css(drupal_get_path('module', 'services_sso_client_profile_flag') . '/css/styles.css');
  drupal_add_js(drupal_get_path('module', 'services_sso_client_profile_flag') . '/js/ui.js');

  $form = array('search_box' => array(), 'tab_controls' => array());

  ahah_helper_register($form, $form_state);
  $all_terms = ProfileFlag::load();
  
  //initialize array of flag ID values
  $form['profileFlag'] = array(
    '#type' => 'value',
    '#value' => array(),
  );

  $actions = array('role_addto', 'role_removefrom', 'group_addto', 'group_removefrom');
  $tabs = array();
  $termTabs = array();

  //submit button for the form
  $form['submit_bottom'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#submit' => array('services_sso_client_profile_flag_actions_submit'),
  );

  //make a fieldset for each flag and store that flag's ID in the form as a value.
  foreach($all_terms as $term) {
    $termID = $term->fid;
    $form['profileFlag']['#value'][$termID] = $termID;

    // Construct the controller tabs set.
    $termTabs[] = $term->string;
    $tabs[] = l($term->string, '', array('attributes' => array('rel' => $term->string, 'class' => 'tab-control tab-control-' . $term->string)));

    $form[$termID] = array(
      '#type' => 'fieldset',
      '#title' => t($term->string),
      '#prefix' => '<div id="'.$termID.'_wrapper" class="tab-content tab-content-' . $term->string . '">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    );

    //initialize variable values
    $addGroupCheck = 0;
    $removeGroupCheck = 0;
    $addRoleCheck = 0;
    $removeRoleCheck = 0;
    $role_add_default = array();
    $role_remove_default = array();
    $group_add_default = array();
    $group_remove_default = array();


    //sets default values on initial load of the form
    foreach ($term->actions as $action) {
      //add roles
      if ($action->type == 'role_addto') {
        $role_add_default = $action->options;
        $addRoleCheck = 1;
        if ($form_state['submitted'] == FALSE) {
          $form_state['values'][$termID][$termID.'_role_addto'] = 1;
        }
      }
      //remove roles
      if ($action->type == 'role_removefrom') { 
        $role_remove_default = $action->options;
        $removeRoleCheck = 1;
        if ($form_state['submitted'] == FALSE) {
          $form_state['values'][$termID][$termID.'_role_removefrom'] = 1;
        }
      }
      //add groups
      if ($action->type == 'group_addto') {
        $group_add_default = $action->options;
        $addGroupCheck = 1;
        if ($form_state['submitted'] == FALSE) {
          $form_state['values'][$termID][$termID.'_group_addto'] = 1;
        }
      }
      //remove groups
      if ($action->type == 'group_removefrom') {
        $group_remove_default = $action->options;
        $removeGroupCheck = 1;
        if ($form_state['submitted'] == FALSE) {
          $form_state['values'][$termID][$termID.'_group_removefrom'] = 1;
        }
      }
    }//foreach, line 97


    /*** Each checkbox and associated action choices handled below. ***/

    //Add Roles
    $form[$termID][$termID.'_role_addto'] = array(
      '#type' => 'checkbox',
      '#title' => 'Add roles',
      '#default_value' => $addRoleCheck,
      '#ahah' => array(
        'path'    => ahah_helper_path(array($termID, $termID.'_role_addto_select')),
        'wrapper' => $termID.'_role_addto_wrapper',
      ),
      '#prefix' => '<div class="checkbox-wrapper">',
      '#suffix' => '</div>',
    );

    $form[$termID][$termID.'_role_addto_select'] = array(
      '#type' => 'hidden',
      '#prefix' => '<div id="'.$termID.'_role_addto_wrapper" class="select-wrapper">',
      '#suffix' => '</div>'
    );
  
    if (!empty($form_state['values'][$termID][$termID.'_role_addto']) && $form_state['values'][$termID][$termID.'_role_addto']) {
      $rolesArray = user_roles(TRUE);
      $form[$termID][$termID.'_role_addto_select'] = array(
        '#type' => 'select',
        '#title' => t('Available Roles'),
        '#options' => $rolesArray,
        '#multiple' => TRUE,
        '#default_value' => $role_add_default,
        '#prefix' => '<div id="'.$termID.'_role_addto_wrapper">',
        '#suffix' => '</div>'
      );
    }

    //Remove Roles
    $form[$termID][$termID.'_role_removefrom'] = array(
      '#type' => 'checkbox',
      '#title' => 'Remove roles',
      '#default_value' => $removeRoleCheck,
      '#ahah' => array(
        'path'    => ahah_helper_path(array($termID, $termID.'_role_removefrom_select')),
        'wrapper' => $termID.'_role_removefrom_wrapper',
      ),
      '#prefix' => '<div class="checkbox-wrapper">',
      '#suffix' => '</div>',
    );

    $form[$termID][$termID.'_role_removefrom_select'] = array(
      '#type' => 'hidden',
      '#prefix' => '<div id="'.$termID.'_role_removefrom_wrapper" class="select-wrapper">',
      '#suffix' => '</div>'
    );

    if (!empty($form_state['values'][$termID][$termID.'_role_removefrom']) && $form_state['values'][$termID][$termID.'_role_removefrom']) {
      $rolesArray = user_roles(TRUE);
      $form[$termID][$termID.'_role_removefrom_select'] = array(
        '#type' => 'select',
        '#title' => t('Available Roles'),
        '#options' => $rolesArray,
        '#multiple' => TRUE,
        '#default_value' => $role_remove_default,
        '#prefix' => '<div id="'.$termID.'_role_removefrom_wrapper">',
        '#suffix' => '</div>'
      );
    }

    //Add Groups
    $form[$termID][$termID.'_group_addto'] = array(
      '#type' => 'checkbox',
      '#title' => 'Add groups',
      '#default_value' => $addGroupCheck,
      '#ahah' => array(
        'path'    => ahah_helper_path(array($termID, $termID.'_group_addto_select')),
        'wrapper' => $termID.'_group_addto_wrapper',
      ),
      '#prefix' => '<div class="checkbox-wrapper">',
      '#suffix' => '</div>',
    );

    $form[$termID][$termID.'_group_addto_select'] = array(
      '#type' => 'hidden',
      '#prefix' => '<div id="'.$termID.'_group_addto_wrapper" class="select-wrapper">',
      '#suffix' => '</div>'
    );

    if (!empty($form_state['values'][$termID][$termID.'_group_addto']) && $form_state['values'][$termID][$termID.'_group_addto']) {
      module_load_include('module', 'og');
      $groupsArray = og_all_groups_options();
      $form[$termID][$termID.'_group_addto_select'] = array(
        '#type' => 'select',
        '#title' => t('Available Groups'),
        '#options' => $groupsArray,
        '#multiple' => TRUE,
        '#default_value' => $group_add_default,
        '#prefix' => '<div id="'.$termID.'_group_addto_wrapper" class="select-wrapper">',
        '#suffix' => '</div>'
      );
    } 

    //Remove Group
    $form[$termID][$termID.'_group_removefrom'] = array(
      '#type' => 'checkbox',
      '#title' => 'Remove groups',
      '#default_value' => $removeGroupCheck,
      '#ahah' => array(
        'path'    => ahah_helper_path(array($termID, $termID.'_group_removefrom_select')),
        'wrapper' => $termID.'_group_removefrom_wrapper',
      ),
      '#prefix' => '<div class="checkbox-wrapper">',
      '#suffix' => '</div>',
    );

    $form[$termID][$termID.'_group_removefrom_select'] = array(
      '#type' => 'hidden',
      '#prefix' => '<div id="'.$termID.'_group_removefrom_wrapper" class="select-wrapper">',
      '#suffix' => '</div>'
    );

    if (!empty($form_state['values'][$termID][$termID.'_group_removefrom']) && $form_state['values'][$termID][$termID.'_group_removefrom']) {

      module_load_include('module', 'og');
      $groupsArray = og_all_groups_options();
      $form[$termID][$termID.'_group_removefrom_select'] = array(
        '#type' => 'select',
        '#title' => t('Available Groups'),
        '#options' => $groupsArray,
        '#multiple' => TRUE,
        '#default_value' => $group_remove_default,
        '#prefix' => '<div id="'.$termID.'_group_removefrom_wrapper">',
        '#suffix' => '</div>'
      );
    }
    $weights = array();
    for ($i = -10; $i <= 10; $i++) {
      $weights[$i] = $i;
    }

    //weight
    $form[$termID][$termID.'_weight'] = array(
      '#type' => 'select',
      '#title' => 'Flag weight',
      '#multiple' => FALSE,
      '#options' => $weights,
      '#prefix' => '<div id="weight-wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $term->weight,
    );

  } // end foreach, line 73

  //defines search box for flags in left column
  $form['search_box'] = array(
    '#type' => 'textfield',
    '#title' => 'Filter Flags',
    '#ahah' => array(
      'path' => ahah_helper_path(array('tab_controls')),
      'wrapper' => 'tab-controls',
      'event' => 'change',
      'effect' => 'fade',
      'keypress' => TRUE,
    ), 
    '#size' => 10,
  );

  //AHAH helper for search box reloads
  if (!empty($form_state['values']['search_box'])) {
    $tabs = array();
    foreach($termTabs as $key => $term) {
      if (strpos($term, trim($form_state['values']['search_box'])) === FALSE) {
        unset($termTabs[$key]);
      } else {
        $tabs[] = l($term, '', array('attributes' => array('rel' => $term, 'class' => 'tab-control tab-control-' . $term)));
      }
    }
  }

  // Deposit the tabs to the front of the $form array.
  $form['tab_controls'] = array(
    '#type' => 'markup',
    '#value' => '<div id="tab-controls" class="tab-controls">' . theme('item_list', $tabs) . '</div>',
  );

  return $form;
}

/**
* As-yet unused helper function for flag action form search area.
* @todo incorporate into search box AHAH helper
*/

function _services_sso_client_profile_flag_search_filter($needle) {
  $search_terms = explode(",", $needle);
  foreach($search_terms as $key => $search) {
    $search_terms[$key] = trim($search);
  }
  return $search_terms;

}

/**
* Submit function for flag actions form above.
*
* the body of the submit is a foreach loop that checks each submitted flag
* and compares each (including selected actions) against existing actions attached
* to the flag.
*/

function services_sso_client_profile_flag_actions_submit($form, &$form_state) {
  services_sso_client_profile_flag_load_includes();
//  dpm($form_state, 'after submit');
  $action_types = array('role_addto', 'role_removefrom', 'group_addto', 'group_removefrom');
  foreach($form_state['values']['profileFlag'] as $flagID) {
    $flag = ProfileFlag::loadByID($flagID);
    $arrays = $form_state['values'][$flagID];
    $flag = current($flag);
    $flag->weight = $arrays[$flagID.'_weight'];
    foreach ($action_types as $action_type) {
      $oldAction = FALSE;
      $oldActionPos = 0;
      foreach($flag->actions as $pos => $old_action) {
        if ($old_action->type == $action_type) {
          $oldAction = $old_action;
          $oldActionPos = $pos;
          break;
        }
      }
      if (!empty($arrays[$flagID.'_'.$action_type]) && $arrays[$flagID.'_'.$action_type]) { //case: action is checked
        if ($oldAction) {
          //the action already exists; update it
          $flag->deleteAction($oldActionPos);
          $oldAction->options = $arrays[$flagID.'_'.$action_type.'_select'];
          $flag->pushAction($oldAction);
        } else {
          //the action doesn't exist; create a new action.

          //check if the selection is empty; no point in creating an action with no options.
          if (!empty($arrays[$flagID.'_'.$action_type.'_select'])) {
            $action = new ProfileFlagAction((object) array('type'=>$action_type, 'fid'=>$flagID, 'options'=>$arrays[$flagID.'_'.$action_type.'_select']));
            $action = $action->map();
            $flag->pushAction($action); 
          }
        }
      } else { //case: action is unchecked. 
        if ($oldAction) {
          //action exists; delete it.
          $flag->deleteAction($oldActionPos);
        }
      }
    } //end foreach, line 271

//  dpm($flag, 'flag_to_save');
  $flag->save();
  }//end foreach, line 266
  drupal_set_message("Flags successfully saved");
} //end function


