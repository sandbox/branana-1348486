<?php

class ProfileFlagActionGroup extends ProfileFlagAction {
  public function actionNamespace() {
    return 'group';
  }

  function apply($uid = 0) {
    // Don't waste time if empty array.
    if (is_array($uid) && empty($uid)) return;

    $actionable = $this->actionable();

    // If uid not set, do it to all users
    if ((empty($uid) && !is_array($uid)) || (!is_numeric($uid) && !is_array($uid))) {
      $uid = array();
      $result = db_query("SELECT uid FROM {users} WHERE 1 ORDER BY uid");
      while ($obj = db_fetch_object($result)) {
        $uid[] = $obj->uid;
      }
    }

    // If we are given an array of uids, break them down and do each individually.
    if (is_array($uid)) {
      foreach ($uid as $individual_uid) {
        $this->apply($individual_uid);
      }
    }
    elseif (is_numeric($uid)) {
      switch ($actionable) {
        case 'addto':
          $now = time();
          foreach ($this->options as $nid) {
            @db_query("INSERT INTO {og_uid} (`uid`, `nid`, `is_active`, `created`, `changed`) VALUES (%d, %d, 1, $now, $now)", $uid, $nid);
          }
          break;
        case 'removefrom':
          foreach ($this->options as $nid) {
            @db_query("DELETE FROM {og_uid} WHERE uid=%d AND nid=%d LIMIT 1", $uid, $nid);
          }
          break;
      }
    }
  }
}