<?php

class ProfileFlagActionRole extends ProfileFlagAction {
  public function actionNamespace() {
    return 'role';
  }

  function apply($uid = 0) {
    // Don't waste time if empty array.
    if (is_array($uid) && empty($uid)) return;
    
    $actionable = $this->actionable(); watchdog('test', 1);

    // If uid not set, do it to all users
    if ((empty($uid) && !is_array($uid)) || (!is_numeric($uid) && !is_array($uid))) {
      $uid = array();
      $result = db_query("SELECT uid FROM {users} WHERE 1 ORDER BY uid");
      while ($obj = db_fetch_object($result)) {
        $uid[] = $obj->uid;
      }
    }

    // If we are given an array of uids, break them down and do each individually.
    if (is_array($uid)) {
      foreach ($uid as $individual_uid) {
        $this->apply($individual_uid);
      }
    }
    elseif (is_numeric($uid)) {
      switch ($actionable) {
        case 'addto':
          foreach ($this->options as $rid) {
            @db_query("INSERT INTO {users_roles} VALUES (%d, %d)", $uid, $rid);
          }
          break;
        case 'removefrom':
          foreach ($this->options as $rid) {
            @db_query("DELETE FROM {users_roles} WHERE uid=%d AND rid=%d LIMIT 1", $uid, $rid);
          }
          break;
      }
    }
  }
}